.ONESHELL:

ENV_NAME=make_ratsql
SHELL = /bin/bash
CONDA_ACTIVATE = source $$(conda info --base)/etc/profile.d/conda.sh ; conda activate ; conda activate
MAIN_DIR = $(shell pwd)/ratsql
CASH_DIR = $(shell pwd)/data



.PHONY: --create_env --download_dependencies --export_environment_variables --download_stanford_nlp activate deactivate delete_env 


all: clean prepare_env activate prepare_datasets run


prepare_env:
	make -- --create_env && \
	make activate && \
	conda install -y pytorch==1.11.0 torchvision==0.12.0 torchaudio==0.11.0 cudatoolkit=11.3 -c pytorch && \
	make -- --download_dependencies && \
	make -- --download_stanford_nlp && \
	make -- --export_environment_variables;


prepare_datasets: activate
	mkdir -p data && \
	gdown --id 1ozbUUpYjhRHj7A7Zd-uPtTgtNRE7s_Os --output ./data/datasets.zip && \
	unzip ./data/datasets.zip -d ./data && \
	cd data && \
	ln -snf ${CASH_DIR}/data/spider spider && \
	ln -snf ${CASH_DIR}/data/wikisql wikisql && \
	cd .. && \
	cp -r data ratsql;


activate:
	$(CONDA_ACTIVATE) $(ENV_NAME)

deactivate:
	source deactivate

delete_env: deactivate
	@if conda env list | grep -q $(ENV_NAME); then \
		conda remove --name $(ENV_NAME) --all; \
	else \
		echo "Environment $(ENV_NAME) does not exist"; \
	fi

preprocess: activate
	cd ratsql;
	python run.py preprocess experiments/spider-glove-run.jsonnet;
	cd ..;

trtain: activate
	cd ratsql;
	python run.py train experiments/spider-glove-run.jsonnet;
	cd ..;

validate: activate
	cd ratsql;
	python run.py eval experiments/spider-glove-run.jsonnet && \
	cd ../output && \
	python get_results.py;
	cd ..;

run: activate
	make preprocess && \
	make train && \
	make validate;

clean:
	make deactivate;
	make delete_env;
	rm -f output/final_results;
	rm -rf ratsql/data;
	rm -rf ratsql/logdir;
	rm -rf data;



--create_env:
	@if conda env list | grep -q $(ENV_NAME); then \
		echo "Environment $(ENV_NAME) already exists"; \
	else \
		conda create --name $(ENV_NAME) -y python=3.8.5; \
	fi

--download_dependencies: activate
	pip install --no-cache-dir -r requirements.txt && \
	python -c "import nltk; nltk.download('stopwords'); nltk.download('punkt')";


--download_stanford_nlp: activate
	mkdir -p ratsql/third_party && \
	cd ratsql/third_party && \
	curl https://download.cs.stanford.edu/nlp/software/stanford-corenlp-full-2018-10-05.zip | jar xv && \
	cd ../..; \

--export_environment_variables: activate
	mkdir -p $$(conda info --envs | grep $(ENV_NAME) | awk '{print $$NF}')/etc/conda/activate.d && \
	mkdir -p $$(conda info --envs | grep $(ENV_NAME) | awk '{print $$NF}')/etc/conda/deactivate.d && \
	echo "export CASH_DIR=$(CASH_DIR)" >> $$(conda info --envs | grep $(ENV_NAME) | awk '{print $$NF}')/etc/conda/activate.d/env_vars.sh;
	

