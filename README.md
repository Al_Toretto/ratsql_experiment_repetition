# Intro:
This repository contains the code necessary to train RAT-SQL (GloVE), on the Sider dataset. 

The code is automated to run outside Docker in a Conda virtural environment. 

# Usage:
### The following are needed to run the code:
 - Conda: Anaconda or Miniconda. 
 - JVM: to run the StanfordNLP.
 - CUDA toolkit > 10.1. 
 - make. 

 ### To run the code write in terminal: 
 make


 ### The results will be in output/final_results.yaml
 [An example of the results is available](output/final_results.yaml)



The RAT-SQL code is here: https://github.com/microsoft/rat-sql