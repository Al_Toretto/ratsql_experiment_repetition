import json
import yaml

results = {}
with open('./../ratsql/logdir/glove_run/bs=20,lr=7.4e-04,end_lr=0e0,att=0/ie_dirs/results.json') as f:
    results = json.load(f)

max_acc = 0
key_max_acc = 0
for k, v in results.items():
    if max_acc <= v['exact']:
        max_acc = v['exact']
        key_max_acc = k

with open ('final_results.yaml', 'w') as f:
    yaml.dump(results[key_max_acc], f, default_flow_style=False, sort_keys=False)
